# custom R2-score metrics for keras backend
from keras import backend as K
import keras

# Se define el coeficiente de determinacion para el backend de Keras
def r2_keras(y_true, y_pred):
    SS_res =  K.sum(K.square( y_true - y_pred )) 
    SS_tot = K.sum(K.square( y_true - K.mean(y_true) ) ) 
    return ( 1 - SS_res/(SS_tot + K.epsilon()) )

def entrenar(tipo_red, X_e, X_v, Y_e, Y_v, verbose, epochs, lr, patience):

    """
    Nombre del archivo: entrenar.py

    Descripción: Genera un modelo neuronal y lo entrena.

    Parámetros:
    tipo_red    : Tipo de red neuronal, puede ser 'clasica' o 'recurrente'.
    X_e         : Subconjunto de entradas de entrenamiento.
    X_v         : Subconjunto de entradas de validacion.
    Y_e         : Subconjunto de salidas de entrenamiento.
    Y_v         : Subconjunto de salidas de validacion.
    verbose     : Cantidad de informacion mostrada por pantalla durante el entrenamiento.
    epochs      : Numero de iteraciones de entrenamiento.
    lr          : Tasa de aprendizaje.
    patience    : Numero de iteraciones, sin mejora, luego de las cuales se detiene el entrenamiento.

    Salida:
    model       : Modelo neuronal entrenado.
    """
    
    import numpy as np
    from keras.models import Sequential, load_model
    from keras.layers import Dense, TimeDistributed, LSTM
    from keras.callbacks import EarlyStopping
    from keras import initializers, metrics, optimizers
    
    # Fijar una semilla para poder reproducir.
    np.random.seed(7)
    
    # Se calculan las neuronas ocultas.
    p_es=X_e.shape[0]
    n_s=1
    n_e=X_e.shape[2]
    neuronas_ocultas = int((p_es-1)*n_s/(n_e+1+n_s))
    
    # Se crea el modelo.
    model = Sequential()
    if tipo_red == 'clasica':
        model.add(TimeDistributed(Dense(neuronas_ocultas, activation='tanh', kernel_initializer='random_uniform', bias_initializer='random_uniform'), input_shape=(X_e.shape[1], X_e.shape[2])))
        model.add(TimeDistributed(Dense(1, activation='tanh', kernel_initializer='random_uniform', bias_initializer='random_uniform')))
    else:
        model.add(LSTM(neuronas_ocultas, activation='tanh', input_shape=(X_e.shape[1], X_e.shape[2]), return_sequences=False, kernel_initializer='random_uniform', bias_initializer='random_uniform'))
        model.add(Dense(1, activation='tanh', kernel_initializer='random_uniform', bias_initializer='random_uniform'))
    
    # Se define la taza de aprendizaje.
    rmsprop = optimizers.RMSprop(lr=lr)
    sgd = optimizers.SGD(lr=lr)
    adam = optimizers.Adam(lr=lr)
    
    # Se compila el modelo.
    model.compile(loss='mean_squared_error', optimizer='adam', metrics=[r2_keras])

    # Se define el criterio para detener el entrenamiento del modelo.
    early_stopping = EarlyStopping(monitor='loss', patience=patience)
    
    # Se entrena el modelo creado.
    model.fit(X_e, Y_e, epochs=epochs, batch_size=X_e.shape[0], verbose=verbose, shuffle=True, validation_data=(X_v, Y_v), callbacks=[early_stopping])
    
    # Se devuelve el nuevo modelo entrenado.
    return model