def descomponerSeries(dataframe, wavelet, nivel):
    
    """
    Nombre del archivo: descomponerSeries.py

    Descripcion: Aplica descomposicion wavelet a las series de un dataframe, utilizando la funcion wavelet y el nivel indicados.

    Parametros:
    dataframe    : Dataframe con las series de tiempo a descomponer.
    waveleet     : Funcion wavelet seleccionada.
    nivel        : Nivel de descomposicion seleccionado.

    Salida:
    dataframe.iloc[:,columnCount:]    : Dataframe generado.
    """
    
    import pandas as pd
    import numpy as np
    import pywt

    # Se guardan los nombres de las columnas y la cantidad de columnas del dataframe.
    columnNames = dataframe.columns
    columnCount = len(columnNames)
    
    # Se descompone cada columna del dataframe.
    for columnName in dataframe.columns:
        
        # Se extrae la señal a descomponer.
        signal = dataframe[columnName].values.reshape(len(dataframe)).tolist()

        # Se obtienen los coeficiente de descomposición de la señal.
        coeffs=pywt.wavedec(signal, wavelet, level=nivel)

        # Se construyen los detalles y la aproximación de la señal a partir de los coeficientes obtenidos.
        for i in range(nivel + 1):
            column = columnName + ' - Detalle_D%s'%(i + 1)
            part = 'd'
            level = i + 1
            take = len(signal)
            if i == nivel:
                column = columnName + ' - Aproximacion_A%s'%i
                part = 'a'
                level = nivel
            wdsig = pywt.upcoef(part = part, coeffs = coeffs[len(coeffs) - 1 - i], wavelet = wavelet, level = level, take = take)
            aux = pd.DataFrame(data = {column: wdsig}, index = dataframe.index)

            # Se genera un dataframe que contiene los detalles y la aproximación.
            dataframe = pd.concat([dataframe, aux], axis = 1)
        
    # Se devuelve el dataframe generado.
    return dataframe.iloc[:,columnCount:]