def prepararES(tipo_red, entradas, salida, ventanas, pasos, wavelet = None, nivel = None, porcentaje_e = 0.66, porcentaje_v = 0.17):
    
    """
    Nombre del archivo: prepararES.py

    Descripción: Prepara las series de tiempo, de entrada y salida, para ser utilizadas en el entrenamiento de los modelos.

    Parámetros:
    tipo_red        : Tipo de red neuronal, puede ser 'clasica' o 'recurrente'.
    entradas        : Lista con los nombres de las series de tiempo de entrada.
    salida          : Lista con el nombre de la serie de tiempo de salida.
    ventanas        : Numero de retardos de tiempo diferido.
    pasos           : Numero de pasos de tiempo a predecir.
    wavelet         : Lista con los nombres de las funciones wavelets, para cada una de las series de entrada.
    nivel           : Nivel de descomposición para las series de entrada.
    porcentaje_e    : Porcentaje de las entradas destinado a entrenamiento.
    porcentaje_v    : Porcentaje de las entradas destinado a validacion.

    Salida:
    X_e        : Subconjunto de entradas de entrenamiento.
    X_v        : Subconjunto de entradas de validación.
    X_p        : Subconjunto de entradas de prueba.
    Y_e        : Subconjunto de salida de entrenamiento.
    Y_v        : Subconjunto de salida de validación.
    Y_p        : Subconjunto de salida de prueba.
    y_scaler   : Escalador para normalizar / desnormalizar un dataframe.
    dfx        : Dataframe con las series de tiempo de entrada.
    Y          : Dataframe con la serie de tiempo de salida.
    """
    
    from importarCSV import importarCSV
    from descomponerSeries import descomponerSeries
    import numpy as np
    import pandas as pd
    from sklearn.preprocessing import MinMaxScaler

    # Se importa el dataframe con las series de tiempo.
    dataframe = importarCSV('005-preprocesados_sin_outliers_y_listos_para_usar')[0]

    # Se seleccionan las series de entrada.
    # Se descomponen las entradas con transformada wavelet si es indicado.
    if (nivel != None):
        aux = pd.DataFrame()
        for entrada in entradas:
            aux = pd.concat([aux, descomponerSeries(dataframe[entradas], wavelet[entradas.index(entrada)], nivel)], axis = 1)
        dfx = aux
    else:
        dfx = dataframe[entradas]
        
    # Se selecciona la serie de salida.
    dfy = dataframe[salida]

    # Se escalan los valores.
    minimo = 0
    maximo = 1
    x_scaler = MinMaxScaler(feature_range=(minimo, maximo))
    y_scaler = MinMaxScaler(feature_range=(minimo, maximo))
    dfvx = x_scaler.fit_transform(dfx.values.tolist())
    dfvy = y_scaler.fit_transform(dfy.values.tolist())

    # Se da formato a los vectores X (entrada), Y (salida).
    # Número de series temporales de entrada.
    features=len(dfvx[0])
    # Entrada
    X = np.array([dfvx[i:i + ventanas + 1] for i in range(0, len(dfvx) - ventanas - pasos)])
    # Salida
    if tipo_red == 'clasica':
        Y = np.array([dfvy[i + pasos:i + ventanas + pasos + 1] for i in range(0, len(dfvx) - ventanas - pasos)])
    else:
        Y = np.array([dfvy[i + ventanas + pasos] for i in range(0, len(dfvx) - ventanas - pasos)])
    
    # Dividir en conjuntos de entrenamiento y pruebas.
    size_e=int(len(X)*porcentaje_e)
    size_v=int(len(X)*porcentaje_v)
    X_e, X_v, X_p=X[:size_e, ], X[size_e:size_e+size_v, ], X[size_e+size_v:, ]
    Y_e, Y_v, Y_p=Y[:size_e, ], Y[size_e:size_e+size_v, ], Y[size_e+size_v:, ]

    return X_e, X_v, X_p, Y_e, Y_v, Y_p, y_scaler, dfx, Y