def graficarDataframe(dataframe):

    """
    Nombre del archivo: graficarDataframe.py

    Descripción: Grafica cada serie de un dataframe por separado.

    Parámetros:
    dataframe: Dataframe que contiene series de tiempo.

    Salida: None
    """

    import matplotlib.pyplot as plt
    import mpld3

    mpld3.enable_notebook()
    axes = dataframe.plot(figsize=(9,4*len(dataframe.columns)), subplots=True, sharex=False, legend=True)
    for i in range(len(dataframe.columns)):
        axes[i].set_ylabel(dataframe.columns[i], labelpad=20)
        axes[i].set_xlabel(dataframe.index.name, labelpad=10)
        axes[i].legend(loc=1, fancybox=True, prop={'size': 12})
    plt.tight_layout()
    plt.show()
    
    return None