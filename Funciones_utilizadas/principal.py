def principal(directorio, tipo_red, entradas, salida, ventanas, pasos, epochs, lr, porcentaje_e, porcentaje_v, patience, verbose, wavelet, nivel, graficar = 0):
    
    """ Descripcion:
        Entrena los modelos, guarda los resultados y grafica las predicciones.
        
        Parametros:
        directorio     : Directorio donde se almacenaran los modelos y resultados.
        tipo_red       : Tipo de red neuronal, puede ser 'clasica' o 'recurrente'.
        entradas       : Lista con los nombres de las series de tiempo de entrada.
        salida         : Lista con el nombre de la serie de tiempo de salida.
        ventanas       : Numero de retardos de tiempo diferido.
        pasos          : Numero de pasos de tiempo a predecir.
        epochs         : Numero de iteraciones de entrenamiento.
        lr             : Tasa de aprendizaje.
        porcentaje_e   : Porcentaje de las entradas destinado a entrenamiento.
        porcentaje_v   : Porcentaje de las entradas destinado a validacion.
        patience       : Numero de iteraciones, sin mejora, luego de las cuales se detiene el entrenamiento.
        verbose        : Cantidad de informacion mostrada por pantalla durante el entrenamiento.
        wavelet        : Lista con los nombres de las funciones wavelets, para cada una de las series de entrada.
        nivel          : Nivel de descomposicion para las series de entrada.
        graficar       : Bandera para graficar o no, las predicciones obtenidas.
        
        Salida:
        df_resultados  : Dataframe con los resultados generados.
    """
    
    from prepararES import prepararES
    from entrenar import entrenar
    from obtenerPredicciones import obtenerPredicciones
    from formatearDatasets import formatearDatasets
    from desnormalizarDatasets import desnormalizarDatasets
    from obtenerRECM import obtenerRECM
    from crearGrafico import crearGrafico
    import os
    
    import pandas as pd
    import h5py, time, datetime, os
    
    # Se crea la lista "resultados" que contendrá los resultados de la evaluación de los modelos.
    resultados = []
    
    # Se crea la estructura de directorios donde se almacenarán los resultados y los modelos.
    ruta = os.environ['MODELOS_PROBADOS']+'/'+directorio+'/'
    if not os.path.exists(ruta):
        os.makedirs(ruta)
    
    # Para cada combinación de parámetros se crea, entrena evalua y almacena el modelo correspondiente y se guardan los resultados de la evaluación.
    # Se preparan las entradas y salidas para los modelos.
    X_e, X_v, X_p, Y_e, Y_v, Y_p, y_scaler, dfx, Y = prepararES(tipo_red, entradas, salida, ventanas, pasos, wavelet, nivel, porcentaje_e, porcentaje_v)
    
    # Se entrena el modelo actual.
    model = entrenar(tipo_red, X_e, X_v, Y_e, Y_v, verbose, epochs, lr, patience)
    
    # Se obtienen las predicciones de entrenamiento y prueba.
    pred_e, pred_v, pred_p = obtenerPredicciones(model, X_e, X_v, X_p)
        
    # Si la red es clásica se da formato a los datos.
    if tipo_red == 'clasica':
        Y_e, Y_v, Y_p = formatearDatasets(Y_e, Y_v, Y_p, ventanas)
        pred_e, pred_v, pred_p = formatearDatasets(pred_e, pred_v, pred_p, ventanas)
        
    # Se desnormalizan los datos.
    Y_e, Y_v, Y_p, pred_e, pred_v, pred_p = desnormalizarDatasets(Y_e, Y_v, Y_p, pred_e, pred_v, pred_p, y_scaler)
    
    # Se obtienen las RECM de entrenamiento, validacion y prueba.
    trainRECM, valRECM, testRECM, R2_p = obtenerRECM(model, Y_e, Y_v, Y_p, pred_e, pred_v, pred_p)
    
    # Se guarda el modelo acutal generando un archivo HDF5 con el formato: "YYYY-MM-DD_H:M:S.h5".
    ts1 = time.time()
    nombre_modelo = datetime.datetime.now().strftime("%H:%M:%S.%f")
    model.save(ruta+nombre_modelo+'.h5')
    
    # Se agregan los resultados del modelo actual a la lista de resultados.
    nueva_fila = [tipo_red, entradas, salida, ventanas, pasos, wavelet, nivel, nombre_modelo, trainRECM, valRECM, testRECM, R2_p]
    resultados.append(nueva_fila)
    
    # Se grafica el modelo actual si es indicado.
    if graficar == 1:
        aux = pd.DataFrame([nueva_fila], columns = ['Tipo de red', 'Entradas', 'Salida', 'Ventanas', 'Pasos', 'Wavelet', 'Nivel de descomposición', 'Nombre del modelo', 'Train RECM', 'Val RECM', 'Test RECM', 'Test R2'])
        display(aux)
        crearGrafico(dfx, ventanas, pasos, Y, Y_e, Y_v, Y_p, pred_e, pred_v, pred_p, salida[0])
    
    # Se elimina el modelo actual.
    del model
    
    # Se genera un dataframe con la lista de resultados.
    df_resultados = pd.DataFrame(resultados, columns = ['Tipo de red', 'Entradas', 'Salida', 'Ventanas', 'Pasos', 'Wavelet', 'Nivel de descomposición', 'Nombre del modelo', 'Train RECM', 'Val RECM', 'Test RECM', 'Test R2'])
    
    # Se ordena el dataframe generado por el campo 'Test RECM' de forma ascendente.
    df_resultados = df_resultados.sort_values(by = ['Test RECM'], ascending = True)
    
    # Se exporta la lista de resultados como csv.
    df_resultados.to_csv(ruta+nombre_modelo+'.csv')

    # Se devuelve el datafreme generado.
    return df_resultados