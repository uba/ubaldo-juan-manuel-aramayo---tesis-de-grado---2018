def encontrarPeriodoCompletoMasGrande(series):

    """
    Nombre del archivo: encontrarPeriodoCompletoMasGrande.py

    Descripción: Encuentra el periodo de tiempo mas grande, donde todos las series de tiempo estén completas y devuelve un conjunto de datos correspondientes a ese periodo.

    Parámetros:
    series: Lista de nombres de las series de tiempo que se desean analizar.

    Salida:
    dataframeSeleccionado: Dataframe que contiene series de tiempo, correspondientes al periodo encontrado.
    """
    
    from importarCSV import importarCSV
    from encontrarPeriodos import encontrarPeriodos
    import numpy as np
    import pandas as pd
    import os

    # Se importan los datos formateados.
    dataframe = importarCSV('003-formateados')[0]
    
    # Se obtienen los periodos completos e incompletos.
    periodos = encontrarPeriodos(dataframe[series])
    
    # Se selecciona el periodo completo más grande.
    periodoCompletoMasGrande = periodos.sort_values(by = ['tamaño'], ascending = False).loc[periodos.sort_values(by = ['tamaño'], ascending = False)['completo']][0:1]
    
    # Se recuperan las fechas de inicio y fin del periodo seleccionado.
    fechaInicio = periodoCompletoMasGrande['fecha inicio'].dt.date.values.astype(str)[0]
    fechaFin = periodoCompletoMasGrande['fecha fin'].dt.date.values.astype(str)[0]
    
    # Se genera un dataframe con los datos seleccionados.
    dataframeSeleccionado = dataframe[series].loc[fechaInicio:fechaFin]
    
    # Se exporta el dataframe generado como CSV.
    dataframeSeleccionado.to_csv(os.environ['DATOS'] + '/004-analizados_y_seleccionados/datosSeleccionados_' + fechaInicio + '_' + fechaFin + '.csv')
    
    # Se devuelve el dataframe generado.
    return dataframeSeleccionado