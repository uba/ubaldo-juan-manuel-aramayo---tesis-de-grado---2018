def formatearDatos():
    
    """
    Nombre del archivo: formatearDatos.py

    Descripcion: Se importan series de tiempo sin formato, se las reindexa para que tengan el mismo tamaño y se las concatena en un mismo dataframe.

    Parametros: None

    Salida:
    dataframeFormateado: Dataframe que contiene series de tiempo, formateadas.
    """
    
    from importarCSV import importarCSV
    import pandas as pd
    import os

    # Se importan los datos sin formato.
    dataframesSinFormato = importarCSV('002-listos_para_importar_y_formatear')

    # Se crea una lista para contener los dataframes formateados.
    dataframesFormateados = []
    
    # Se formatea cada dataframe y se lo agrega a la lista de dataframes formateados.
    for dataframe in dataframesSinFormato:
        # Si existen varios datos para un mismo dia, se los agrupa y se guarda solo el promedio.
        dataframe = dataframe.groupby(dataframe.index).mean()
        # Se reindexa en un periodo que incluye las mediciones de todos los datos obtenidos.
        dataframe = dataframe.reindex(pd.date_range('1947-01-01', '2016-12-31'))
        # Se agrega el dataframe a la lista.
        dataframesFormateados.append(dataframe)
        
    # Se unen todos los dataframes formateados.
    dataframeFormateado = pd.concat(dataframesFormateados, axis = 1)
    
    # Se asigna el nombre al índice del dataframe generado.
    dataframeFormateado.index.name = dataframesSinFormato[0].index.name
    
    # Se exporta el dataframe generado como CSV.
    dataframeFormateado.to_csv(os.environ['DATOS'] + '/003-formateados/datosFormateados_1947-2016.csv')
    
    # Se devuelve el dataframe generado.
    return dataframeFormateado