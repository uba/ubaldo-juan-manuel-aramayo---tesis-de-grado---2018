def formatearDatasets(Y_e, Y_v, Y_p, ventanas):

    """
    Nombre del archivo: formatearDatasets.py

    Descripción: Formatea los datasets objetivos, para poder desnormalizarlos.

    Parámetros:
    Y_e        : Subconjunto de entrenamiento.
    Y_v        : Subconjunto de validacion.
    Y_p        : Subconjunto de prueba.
    ventanas   : Numero de retardos de tiempo diferido.

    Salida:
    Y_e        : Subconjunto de entrenamiento formateado.
    Y_v        : Subconjunto de validacion formateado.
    Y_p        : Subconjunto de prueba formateado.
    """
    
    import numpy as np

    # Se da formato a los datasets, para poder desnormalizarlos.
    Y_e_aux=Y_e
    Y_e=np.array([Y_e_aux[i][ventanas-1] for i in range(0, len(Y_e_aux))])
    Y_v_aux=Y_v
    Y_v=np.array([Y_v_aux[i][ventanas-1] for i in range(0, len(Y_v_aux))])
    Y_p_aux=Y_p
    Y_p=np.array([Y_p_aux[i][ventanas-1] for i in range(0, len(Y_p_aux))])
    
    # Se devuelven los datasets formateados.
    return Y_e, Y_v, Y_p