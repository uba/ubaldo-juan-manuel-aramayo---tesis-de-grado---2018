def desnormalizarDatasets(Y_e, Y_v, Y_p, pred_e, pred_v, pred_p, y_scaler):

    """
    Nombre del archivo: desnormalizarDatasets.py

    Descripción: Desnormaliza las predicciones y las series de tiempo objetivo.

    Parámetros:
    Y_e        : Subconjunto de entrenamiento.
    Y_v        : Subconjunto de validacion.
    Y_p        : Subconjunto de prueba.
    pred_e     : Predicciones de entrenamiento.
    pred_v     : Predicciones de validacion.
    pred_p     : Predicciones de prueba.
    y_scaler   : Escalador para normalizar / desnormalizar un dataframe.

    Salida:
    Y_e        : Subconjunto de entrenamiento desnormalizado.
    Y_v        : Subconjunto de validacion desnormalizado.
    Y_p        : Subconjunto de prueba desnormalizado.
    pred_e     : Predicciones de entrenamiento desnormalizado.
    pred_v     : Predicciones de validacion desnormalizado.
    pred_p     : Predicciones de prueba desnormalizado.
    """
    
    # Se desnormalizan los datasets.
    pred_e=y_scaler.inverse_transform(pred_e)
    Y_e=y_scaler.inverse_transform(Y_e)
    pred_v=y_scaler.inverse_transform(pred_v)
    Y_v=y_scaler.inverse_transform(Y_v)
    pred_p=y_scaler.inverse_transform(pred_p)
    Y_p=y_scaler.inverse_transform(Y_p)
    
    # Se devuelven los datasets desnormalizados.
    return Y_e, Y_v, Y_p, pred_e, pred_v, pred_p