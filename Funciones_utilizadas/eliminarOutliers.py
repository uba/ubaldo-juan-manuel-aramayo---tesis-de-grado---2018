def eliminarOutliers(columna):
    
    """
    Nombre del archivo: eliminarOutliers.py

    Descripción: Elimina outliers, de las series de tiempo contenidas en un dataframe, utilizando el método de las fronteras internas y externas.

    Parámetros:
    columna: Nombre de la serie de tiempo a la cual se le eliminaran los outliers.

    Salida:
    dataframe: Dataframe con la serie de tiempo generada.
    """
    
    from importarCSV import importarCSV
    import numpy as np
    import pandas as pd
    import os
    import matplotlib.pyplot as plt
    import matplotlib
    import mpld3

    # Se importan los datos previamente seleccionados.
    dataframe = importarCSV('004-analizados_y_seleccionados')[0]
    
    ql = dataframe[[columna]].sort_values(by = [columna], ascending = True).describe().iloc[[4, 6], :].values.tolist()[0][0]
    qu = dataframe[[columna]].sort_values(by = [columna], ascending = True).describe().iloc[[4, 6], :].values.tolist()[1][0]
    iqr = qu-ql
    in1 = ql-1.5*iqr
    in2 = qu+1.5*iqr

    dataframe['Outliers'] = np.nan
    dataframe[columna+'-sin Outliers'] = dataframe[columna]
    for i in range(dataframe.index.size):
        if dataframe[columna][i]<in1 or dataframe[columna][i]>in2:
            dataframe['Outliers'][i]=dataframe[columna][i]
            dataframe[columna+'-sin Outliers'][i]=np.nan
    dataframe[columna+'-sin Outliers']=dataframe[columna+'-sin Outliers'].interpolate()
    dataframe[[columna, 'Outliers', columna+'-sin Outliers']].plot(style=['-','.', 'r-'], figsize=(9,5))
    plt.show()
    
    fechaInicio = dataframe.index.astype(str)[0]
    fechaFin = dataframe.index.astype(str)[-1]
    
    # Se exporta el dataframe generado como CSV.
    dataframe.to_csv(os.environ['DATOS'] + '/005-preprocesados_sin_outliers_y_listos_para_usar/datosSinOutliers_' + fechaInicio + '_' + fechaFin + '.csv')

    
    # Se devuelve el dataframe generado.
    return dataframe