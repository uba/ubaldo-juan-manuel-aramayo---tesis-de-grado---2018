def crearGrafico(dfx, ventanas, pasos, Y, Y_e, Y_v, Y_p, pred_e, pred_v, pred_p, ylabel):

    """
    Nombre del archivo: crearGrafico.py

    Descripción: Genera un dataframe con las predicciones y con los datos reales y los imprime en un mismo grafico.

    Parámetros:
    dfx        : Dataframe con las series de tiempo de entrada.
    Ventanas   : Numero de retardos de tiempo diferido.
    Pasos      : Numero de pasos de tiempo a predecir.
    Y          : Dataframe con la serie de tiempo de salida.
    Y_e        : Subconjunto de entrenamiento.
    Y_v        : Subconjunto de validacion.
    Y_p        : Subconjunto de prueba.
    pred_e     : Predicciones de entrenamiento.
    pred_v     : Predicciones de validacion.
    pred_p     : Predicciones de prueba.
    ylabel     : Nombre de la serie de tiempo de salida.

    Salida: None.
    """
    
    import numpy as np
    import pandas as pd
    import matplotlib.pyplot as plt
    import mpld3

    # Se preparan los datos para graficar.

    # Se definen las fechas necesarias para graficar.
    fechas=dfx.index[ventanas-1+pasos:ventanas-1+pasos+len(Y)]
    fechas_e=fechas[:len(Y_e)]
    fechas_v=fechas[len(fechas_e):len(fechas_e)+len(Y_v)]
    fechas_p=fechas[len(fechas_e)+len(fechas_v):]

    # Se construyen los dataframes.
    dfPred_e=pd.DataFrame(data=np.reshape(pred_e, (len(pred_e),)), index=fechas_e, columns=['Predicciones de entrenemiento'])
    dfY_e=pd.DataFrame(data=np.reshape(Y_e, (len(Y_e),)), index=fechas_e, columns=['Objetivos de entrenemiento'])
    
    dfPred_v=pd.DataFrame(data=np.reshape(pred_v, (len(pred_v),)), index=fechas_v, columns=['Predicciones de validación'])
    dfY_v=pd.DataFrame(data=np.reshape(Y_v, (len(Y_v),)), index=fechas_v, columns=['Objetivos de validación'])
    
    dfPred_p=pd.DataFrame(data=np.reshape(pred_p, (len(pred_p),)), index=fechas_p, columns=['Predicciones de prueba'])
    dfY_p=pd.DataFrame(data=np.reshape(Y_p, (len(Y_p),)), index=fechas_p, columns=['Objetivos de prueba'])

    # Se reindexan los dataframes, para poder unirlos.
    dfPred_e=dfPred_e.reindex(fechas)
    dfY_e=dfY_e.reindex(fechas)
    dfPred_p=dfPred_p.reindex(fechas)
    dfY_p=dfY_p.reindex(fechas)

    # Se unen los dataframes.
    resultado=pd.concat([dfY_e, dfPred_e, dfY_v, dfPred_v, dfY_p, dfPred_p], axis=1)

    # Se realiza el grafico.
    mpld3.enable_notebook()
    ax = resultado.plot(style = ['C0-','C1-', 'C2-','C3-', 'C8-','C9-'], figsize = (12,6))
    ax.set_ylabel(ylabel, labelpad=20)
    ax.set_xlabel('Fecha', labelpad=8)
    if ylabel!='Caudal[m3/s]':
        ax.set_ylim([dfY_e.min().values[0]*0.9, dfY_e.max().values[0]*1.3])
    else:
        ax.set_ylim([dfY_e.min().values[0]*0, dfY_e.max().values[0]*1.6])
    ax.legend(loc=2, fancybox=True, prop={'size': 12})
    plt.tight_layout()
    plt.show()
    
    return None