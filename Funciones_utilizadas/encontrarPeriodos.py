def encontrarPeriodos(dataframe):
    
    """
    Nombre del archivo: encontrarPeriodos.py

    Descripción: Agrupa las series de tiempo de un dataframe y genera un dataframe de periodos de tiempo especificando si están completos o no.

    Parámetros:
    dataframe: Dataframe que contiene series de tiempo.

    Salida:
    periodos: Dataframe de periodos generado.
    """
    
    import numpy as np
    import pandas as pd

    # Se crea un dataset que contendrá los periodos encontrados.
    periodos=pd.DataFrame()
    
    # Columna 'completo' = True si nigún valor es nulo en la fila.
    periodos['completo'] = dataframe[dataframe.columns[:]].notnull().all(axis=1)

    # Columna 'periodo' para identificar cada periodo con un número entero, se inicializa en 0.
    periodos['periodo'] = np.zeros(periodos.size).astype(int)
    
    # Al primer registro de cada periodo se asigna 'periodo' = 1.
    for i in range(1, periodos.index.size):
        if periodos['completo'][i]!=periodos['completo'][i-1]:
            periodos['periodo'][i]=1
    
    # Se realiza una sumatoria acumulada sobre la columna 'periodo', para que cada registro quede identificado con su número de periodo.
    periodos['periodo'] = periodos['periodo'].cumsum()
    
    # Se reindexa el dataset con un indice por defecto.
    periodos = periodos.reset_index()
    
    # Se agregan las columnas 'fecha inicio' 'fecha fin' para cada periodo.
    periodos.columns = ['fecha inicio', 'completo', 'periodo']
    periodos['fecha fin'] = periodos['fecha inicio']
    
    # Se reagrupa el dataframe, para cada periodo se determina si esta completo, el tamaño, la fecha de inicio y la fecha de fin.
    periodos = periodos.groupby(['periodo']).aggregate({'completo':{'completo':'min'},'periodo':{'tamaño':'count'},'fecha inicio':{'fecha inicio':'min'},'fecha fin':{'fecha fin':'max'}})
    periodos.columns = periodos.columns.droplevel()
    
    # Se ordena de mayor a menor segun el tamaño de los periodos y se muestra por pantalla.
    periodos
    
    # Se devuelve el dataframe generado.
    return periodos