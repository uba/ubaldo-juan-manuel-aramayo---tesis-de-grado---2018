def importarCSV(subdirectorio = '005-preprocesados_sin_outliers_y_listos_para_usar'):
    
    """
    Nombre del archivo: importarCSV.py

    Descripción: Importa series de tiempo de multiples archivos CSV, en un dataframe para cada una.

    Parámetros:
    subdirectorio     : Nombre del directorio donde se encuentran los archivos CSV.

    Salida:
    dataframes        : Lista con los dataframes generados.
    """
    
    import os
    import glob
    import pandas as pd

    # Se define el directorio que contiene los CSVs a importar.
    directorio = os.environ['DATOS'] + '/' + subdirectorio
    
    # Se crea una lista con los nombres de cada CSV.
    listaCSV = glob.glob(directorio + "/*.csv")
    
    # Se importa cada CSV como dataframe, por separado y se crea una lista con todos los dataframes.
    dataframes = [pd.read_csv(csv, engine='python', index_col=0, parse_dates=True, infer_datetime_format=True, dayfirst=True) for csv in listaCSV]
    
    # Se devuelve la lista con los dataframes generados.
    return dataframes
