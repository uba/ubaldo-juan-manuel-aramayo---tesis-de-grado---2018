def obtenerRECM(model, Y_e, Y_v, Y_p, pred_e, pred_v, pred_p):

    """
    Nombre del archivo: obtenerRECM.py

    Descripción: Obtiene la Raiz del Error Cuadratico Medio para los conjuntos de entrenamiento, validacion y prueba. Tambien obtiene el coeficiente de determinacion para el conjunto de prueba.

    Parámetros:
    model      : Modelo neuronal entrenado.
    Y_e        : Subconjunto de entrenamiento.
    Y_v        : Subconjunto de validacion.
    Y_p        : Subconjunto de prueba.
    pred_e     : Predicciones de entrenamiento.
    pred_v     : Predicciones de validacion.
    pred_p     : Predicciones de prueba.

    Salida:
    trainRECM  : Raiz del Error Cuadratico Medio para entrenamiento.
    valRECM    : Raiz del Error Cuadratico Medio para validacion.
    testRECM   : Raiz del Error Cuadratico Medio para prueba.
    R2_p       : Coeficiente de determinacion para prueba.
    """
    
    import math
    import numpy as np
    from sklearn.metrics import mean_squared_error

    # Se obtiene la Raiz del Error Cuadratico Medio de entrenamiento, validacion y prueba.
    trainRECM = np.sqrt(np.average(np.square(Y_e - pred_e)))
    valRECM = np.sqrt(np.average(np.square(Y_v - pred_v)))
    testRECM = np.sqrt(np.average(np.square(Y_p - pred_p)))
    
    # Se obtiene el coefiente de determinacion para el conjunto de prueba.
    R2_p = np.square(np.corrcoef(Y_p.reshape(len(Y_p)), pred_p.reshape(len(pred_p)))[0][1])
    
    # Se devuelven las metricas obtenidas.
    return trainRECM, valRECM, testRECM, R2_p