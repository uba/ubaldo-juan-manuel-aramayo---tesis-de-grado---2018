def obtenerPredicciones(model, X_e, X_v, X_p):

    """
    Nombre del archivo: obtenerPredicciones.py

    Descripción: Obtiene las predicciones de entrenamiento, validacion y prueba.

    Parámetros:
    X_e        : Subconjunto de entradas de entrenamiento.
    X_v        : Subconjunto de entradas de validacion.
    X_p        : Subconjunto de entradas de prueba.

    Salida:
    pred_e     : Predicciones de entrenamiento.
    pred_v     : Predicciones de validacion.
    pred_p     : Predicciones de prueba.
    """
    
    import numpy as np
    
    # Se obtienen las predicciones de entrenamiento, validacion y prueba.
    pred_e=model.predict(X_e)
    pred_v=model.predict(X_v)
    pred_p=model.predict(X_p)
    
    # Se devuelven las predicciones de entrenamiento, validacion y prueba.
    return pred_e, pred_v, pred_p