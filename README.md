UNIVERSIDAD NACIONAL DE JUJUY

FACULTAD DE INGENIERÍA

&nbsp;

TRABAJO FINAL DE GRADO

Ingeniería Informática
&nbsp;

&nbsp;

**Aplicación de Redes Neuronales Artificiales: Tratamiento de series temporales
estacionales para predicción de alturas y caudales en Caimancito, Jujuy,
Argentina.**
&nbsp;

**Anexo A - Funciones utilizadas**

Todas las funciones se pueden obtener del siguiente enlace:
https://gitlab.com/uba/ubaldo-juan-manuel-aramayo---tesis-de-grado---2018
&nbsp;

&nbsp;

Autor: Ubaldo J. M. Aramayo.

Director: Sergio L. Martínez.

Codirector: Susana Chalabe.
&nbsp;

&nbsp;

San Salvador de Jujuy. 2018